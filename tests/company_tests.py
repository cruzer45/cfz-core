import unittest
from nose.tools import eq_, raises

from collections import namedtuple
from datetime import datetime

from cfzcore.company import Company, CompanyType, CompanyParams
from cfzcore.company import InvalidNameException 
from cfzcore.person import Person


class CompanyTests(unittest.TestCase):
    def setUp(self):
        self.registration_number = "IN032443534"
        self.investor1 = Person("Investor1", "Last")
        self.investor2 = Person("Investor2", "Last")
        params = CompanyParams(name="Name", registration_number=self.registration_number)
        self.company = Company(params)

    def test_create_company(self):
        #print "company: ", self.company.__tuple__
        eq_("Name", self.company.name)
        eq_(self.registration_number, self.company.registration_number)

    @raises(InvalidNameException)
    def test_should_raise_exception_if_name_is_empty(self):
        CompanyParameters = namedtuple("CompanyParameters", "name registration_number")
        params = CompanyParameters(name="", registration_number="")
        Company(params)

    @raises(InvalidNameException)
    def test_should_raise_exception_if_registration_number_is_empty(self):
        CompanyParameters = namedtuple("CompanyParameters", "name registration_number")
        params = CompanyParameters(name="Name", registration_number="")
        Company(params)

    def test_should_assign_a_telephone_number(self):
        self.company.telephone_number = 1233434
        eq_(1233434, self.company.telephone_number)

    def test_should_assign_shareholder(self):
        #Make sure the investors list is empty
        if getattr(self.company, 'shareholders', None):
            del self.company.shareholders[:]
            eq_(0, len(self.company.shareholders))
        params = dict(person=self.investor1, shares=50)
        self.company.assign_shareholder(**params)
        eq_(1, len(self.company.shareholders))

    def test_obtain_an_investors_shares_percentage(self):
        if getattr(self.company, 'shareholders', None):
            del self.company.shareholders[:]
        params = dict(person=self.investor1, shares=70)
        self.company.assign_shareholder(**params)
        params = dict(person=self.investor2, shares=30)
        self.company.assign_shareholder(**params)
        eq_(30, self.company.shareholder_shares_for(self.investor2))

    def test_should_update_contact_information(self):
        contact_numbers = {'telephone_number': '1234', 'fax_number': '0001'}
        self.company.update_contact_information(**contact_numbers)
        eq_('1234', self.company.telephone_number)
        eq_('0001', self.company.fax_number)

    def test_convert_company__tuple__(self):
        company_dict = self.company.__tuple__
        eq_('Name', company_dict.name)

    def test_should_replace_director(self):
        director_information = {'first_name': 'Replace', 'last_name': 'Name'}
        director = Person(**director_information)
        self.company.replace_director(director)
        eq_('Replace', self.company.director.first_name)
        eq_('Name', self.company.director.last_name)

    def test_should_update_general_information(self):
        general_information = {'registration_number': '2',
                               'name': 'New Name',
                               'company_type': 'Manufacturing'}
        self.company.update_general_information(**general_information)
        eq_('2', self.company.registration_number)
        eq_('New Name', self.company.name)
        eq_('Manufacturing', self.company.company_type)

    def test_should_retrieve_created_time(self):
        self.company.created_date = datetime.strptime("01/12/2012", "%d/%m/%Y")
        eq_("01/12/2012", self.company.get_created_date())

    def test_should_update_company_country(self):
        self.company.update_country(**dict(state="New State",
                                           country="New Country"))
        eq_("New State", self.company.state)
        eq_("New Country", self.company.country)

    def test_should_update_company_contact_numbers(self):
        contact_numbers = dict(telephone_number="1234", fax_number="5678",
                               cell_number="9012")
        self.company.update_contact_numbers(**contact_numbers)
        eq_("1234", self.company.telephone_number)
        eq_("5678", self.company.fax_number)
        eq_("9012", self.company.cell_number)
