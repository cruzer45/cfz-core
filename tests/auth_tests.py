import unittest
from mock import Mock
from nose.tools import eq_
from cfzcore.user_account import UserAccount, UserGroup
from cfzcore.auth import groupfinder


class AuthTests(unittest.TestCase):
    def test_should_return_groups_user_belongs_to(self):
        user_account = UserAccount(u'Test User', u'user@mail.com')
        group = UserGroup('group')
        user_account.add_to_group(group)
        request = Mock()
        groups = groupfinder(user_account)
        eq_(['g:group'], groups)
