import unittest
from nose.tools import eq_

from mock import Mock

from cfzcore.application import ApplicationInvestor


class ApplicationInvestorTests(unittest.TestCase):

    def setUp(self):
        self.application = Mock()
        self.person = Mock()
        self.investor = ApplicationInvestor(self.person, 50, self.application)

    def test_should_return_False_if_background_check_failed(self):
        self.investor.background_check = 'F'
        eq_(False, self.investor.has_passed_background_check())

    def test_should_return_True_if_background_check_passed(self):
        self.investor.background_check = 'P'
        eq_(True, self.investor.has_passed_background_check())

    def test_pass_background_check(self):
        self.investor.do_background_check('P')
        eq_('P', self.investor.background_check)

    def test_fail_background_check(self):
        self.investor.do_background_check('F')
        eq_('F', self.investor.background_check)
