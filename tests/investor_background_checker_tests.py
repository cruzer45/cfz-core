import unittest

from mock import Mock

from cfzcore import InvestorBackgroundChecker


class InvestorBackgroundCheckerTests(unittest.TestCase):

    def setUp(self):
        self.investor = Mock()
        self.investor_id = 1
        self.repository = Mock()
        self.repository.get.return_value = self.investor
        self.checker = InvestorBackgroundChecker(self.repository)

    def test_should_pass_background_check(self):
        self.checker.save_background_check(self.investor_id, 'P')
        self.investor.do_background_check.assert_called_once_with('P')
        self.repository.get.assert_called_once_with(self.investor_id)
        self.repository.persist.assert_called_once_with(self.investor)

    def test_should_fail_background_check(self):
        self.checker.save_background_check(self.investor_id, 'F')
        self.investor.do_background_check.assert_called_once_with('F')
        self.repository.get.assert_called_once_with(self.investor_id)
        self.repository.persist.assert_called_once_with(self.investor)
