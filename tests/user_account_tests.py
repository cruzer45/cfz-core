import unittest
from nose.tools import eq_
from cfzcore.user_account import UserAccount, UserGroup


class UserAccountTests(unittest.TestCase):

    def setUp(self):
        self.user = UserAccount("john doe", "john@mail.com")
        self.clerk_group = UserGroup('clerks')
        self.admin_group = UserGroup('admin')

    def test_should_hash_a_password(self):
        password = 'unhashedpassword'
        self.assertNotEqual(password, self.user.hash_password(password))

    def test_passwords_should_be_equal(self):
        newpassword = 'newpassword'
        self.user.set_password(newpassword)
        self.assertTrue(self.user.check_password(newpassword))

    def test_check_password_should_be_false_when_passwords_are_not_equal(self):
        realpassword = 'realpassword'
        self.user.set_password(realpassword)
        self.assertFalse(self.user.check_password('fakepassword'))

    def test_should_assign_user_to_a_group(self):
        self.user.add_to_group(self.clerk_group)
        self.assertTrue(self.clerk_group in self.user.groups)

    def test_should_assign_multiple_groups_to_a_user(self):
        del self.user.groups[:]
        self.user.add_to_group(self.clerk_group)
        self.user.add_to_group(self.admin_group)
        self.assertTrue(self.clerk_group in self.user.groups)
        self.assertTrue(self.admin_group in self.user.groups)
        eq_(2, len(self.user.groups))
