import unittest
from cfzcore.address import Address
from cfzcore.outlet import (Outlet, OutletManager)
from cfzcore.builders.outlets import OutletBuilder
from nose.tools import eq_


class OutletBuilderTests(unittest.TestCase):
    def test_should_add_address_to_outlet(self):
        outlet = Outlet('Outlet')
        builder =  OutletBuilder(outlet)
        address = Address()
        address.update("Plaza", "Locale")
        builder.add_address(address)
        eq_(address, outlet.address)

    def test_should_add_manager(self):
        outlet = Outlet('Outlet')
        builder = OutletBuilder(outlet)
        manager = OutletManager("First", "Last")
        builder.add_manager(manager)
        eq_(1, len(outlet.managers))


    def _create_outlet(self, name):
        return Outlet(name)


