from nose.tools import eq_
import unittest
from mock import Mock

from cfzcore.interactors.company import create_annual_report_for_company


class AddCompanyAnnualReportTests(unittest.TestCase):

    def test_should_add_annual_report_for_company(self):
        company = Mock()
        company_id = 1
        repository = Mock()
        repository.get.return_value = company
        report = dict(date_submitted="21/01/2012",
                      reporting_year=2011)
        annual_report = create_annual_report_for_company(company, **report)
        eq_(company, annual_report.company)
