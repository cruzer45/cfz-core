import unittest
from nose.tools import eq_
from mock import Mock

from cfzcore.shareholder import Shareholder
from cfzcore.interactors.company import CompanyShareholder


class CompanyShareholderTests(unittest.TestCase):

    def setUp(self):
        self.company = Mock()
        self.shareholder1 = Shareholder(**dict(id=1,
                                               first_name="First",
                                               last_name="Last",
                                               background_check="P",
                                               company=self.company,
                                               shares=35))
        shareholder2 = Shareholder(**dict(id=2,
                                          first_name="Second",
                                          last_name="Last",
                                          background_check="P",
                                          company=self.company,
                                          shares=45))
        self.company.shareholders = [self.shareholder1, shareholder2]
        self.company.__tuple__ = None
        self.company_id = 1
        self.repository = Mock()
        self.repository.get.return_value = self.company
        self.repository.get_shareholder_by_id.return_value = self.shareholder1
        self.company_shareholder = CompanyShareholder(self.company_id, self.repository)

    def test_should_calculate_total_shares_owned(self):
        eq_(80, self.company_shareholder.total_shares_owned())

    def test_should_upgrade_shares(self):
        shareholder = self.__upgrade_shares__()
        eq_(55, shareholder.shares)
        eq_("Major", shareholder.person.first_name)
        eq_("Last", shareholder.person.last_name)

    def __upgrade_shares__(self):
        shareholder = self.company_shareholder.save_shareholder_by_id(1,
                                                                      **dict(shares=55,
                                                                      first_name="Major"))
        self.repository.get_shareholder_by_id.assert_called_once_with(1)
        self.repository.persist.assert_called_once_with(self.shareholder1)
        return shareholder

    def test_should_add_new_shareholder(self):
        self.company_shareholder.add_shareholder(**dict(shares=10,
                                                        first_name="Third",
                                                        last_name="Shareholder"))
        eq_(90, self.company_shareholder.total_shares_owned())
        self.repository.persist.assert_called_once_with(self.company)

    def test_retrieve_company_shareholders(self):
        self.repository.get_shareholders_for_company.return_value = self.company.shareholders
        self.company_shareholder.all_shareholders()
        self.repository.get_shareholders_for_company.assert_called_once_with(self.company_id)
