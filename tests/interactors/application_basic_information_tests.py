import unittest
from nose.tools import eq_
from mock import Mock
from collections import namedtuple

from cfzcore.interactors.application import ApplicationBasicInformation
from cfzcore.application import Application
from cfzcore.company import Company
from cfzcore.person import Person


class ApplicationBasicInformationTests(unittest.TestCase):

    def setUp(self):
        CompanyParams = namedtuple("CompanyParams", "name registration_number\
                                                    manager company_type")
        manager = Person(first_name="First", last_name="Manager")
        company_params = CompanyParams(name="Company", registration_number="1",
                                            manager=manager,
                                            company_type="Import/Export")
        company = Company(company_params)
        self.application = Application(**dict(company=company))
        self.application_id = 1
        self.repository = Mock()
        self.basic_information_form = ApplicationBasicInformation(self.repository)

    def test_update_application_basic_information(self):
        self.repository.get.return_value = self.application
        params = {'name': 'New Company Name',
                  'registration_number': '2',
                  'company_type': 'Manufacturing',
                  'application_id': self.application_id}
        application_result_model = self.basic_information_form.update_basic_information(**params)
        self.repository.get.assert_called_once_with(self.application_id)
        self.repository.persist.assert_called_once_with(self.application)
        self.__assert_company_updated__(application_result_model)

    def __assert_company_updated__(self, application):
        eq_('New Company Name', application.company.name)
        eq_('2', application.company.registration_number)

    def test_save_contact_information(self):
        params = {'telephone_number': '1234',
                  'fax_number': '5678',
                  'cable_number': '5677',
                  'application_id': 1}
        self.repository.get.return_value = self.application
        self.basic_information_form.save_contact_numbers(**params)
        self.repository.get.assert_called_once_with(self.application_id)
        self.repository.persist.assert_called_once_with(self.application)
        self.__assert_contact_numbers_updated()

    def __assert_contact_numbers_updated(self):
        eq_("1234", self.application.company.telephone_number)

    def test_save_company_location(self):
        params = dict(state="Cayo", country="Belize", application_id=self.application_id)
        self.repository.get.return_value = self.application
        self.basic_information_form.save_company_location(**params)
        self.repository.get.assert_called_once_with(self.application_id)
        self.repository.persist.assert_called_once_with(self.application)
        self.__assert_location_updated()

    def __assert_location_updated(self):
        eq_("Cayo", self.application.state)
        eq_("Belize", self.application.country)

    def test_save_description_of_goods(self):
        params = dict(description_of_goods="Description",
                      application_id=self.application_id)
        self.repository.get.return_value = self.application
        self.basic_information_form.save_description_of_goods(**params)
        self.repository.get.assert_called_once_with(self.application_id)
        self.repository.persist.assert_called_once_with(self.application)
        eq_("Description", self.application.description_of_goods)
