import unittest
from nose.tools import eq_
from mock import Mock

from cfzcore.company import Company, CompanyType, CompanyParams
from cfzcore.interactors.company import CompanyCountry


class CompanyCountryTests(unittest.TestCase):
    def setUp(self):
        params = CompanyParams(name="Company", registration_number="1")
        self.company = Company(params)
        self.company_id = 1
        self.repository = Mock()
        self.repository.get.return_value = self.company

    def test_should_update_address(self):
        company_address = CompanyCountry(self.company_id, self.repository)
        company_address.update(**dict(state="Corozal",
                                      country="Belize"))
        eq_("Corozal", self.company.state)
        eq_("Belize", self.company.country)
        self.repository.get.assert_called_once_with(self.company_id)
        self.repository.persist.assert_called_once_with(self.company)
