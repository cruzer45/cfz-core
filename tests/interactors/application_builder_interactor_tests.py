import unittest
from nose.tools import eq_, raises
from mock import Mock

from collections import namedtuple

from cfzcore.interactors.application_builder import ApplicationFormBuilder
from cfzcore import InvestorBackgroundCheck
from cfzcore.interactors.application_builder import FormIncompleteException


class ApplicationBuilderInteractorTests(unittest.TestCase):

    def setUp(self):
        self.repository = Mock()
        self.__add_background_checks()
        self.params = {'name': 'Company',
                       'company_type': 'Import/Export',
                       'registration_number': '1',
                       'first_name': 'First',
                       'last_name': 'Manager',
                       'submission_date': '30/01/2012'}
        self.builder = ApplicationFormBuilder(self.repository)
        self.application = self.builder.initialize_application_form(**self.params)
        self.application_id = 1
        self.application.remove_all_investors()
        self.repository.get.return_value = self.application

    def test_application_is_initialized(self):
        eq_('Company', self.application.company.name)
        eq_('30/Jan/2012', self.application.get_submission_date())

    @raises(FormIncompleteException)
    def test_verify_application_form_should_fail_when_telephone_number_is_None(self):
        self.builder.validate_form_is_complete(self.application)

    def test_verify_application_form_validation_error_when_telephone_number_is_None(self):
        try:
            self.builder.validate_form_is_complete(self.application)
        except FormIncompleteException, (instance):
            self.assertIsNotNone(instance.error)
            self.assertIsNotNone(instance.error.telephone_number)

    @raises(FormIncompleteException)
    def test_verify_aplication_form_should_fail_when_place_of_origin_is_None(self):
        self.builder.validate_form_is_complete(self.application)

    def test_verify_application_form_validation_error_when_place_of_origin_is_None(self):
        try:
            self.builder.validate_form_is_complete(self.application)
        except FormIncompleteException, (instance):
            self.assertIsNotNone(instance.error)
            self.assertIsNotNone(instance.error.place_of_origin)
            eq_('New', self.application.status)

    def test_update_application_basic_information(self):
        params = {'name': 'New Company Name',
                  'registration_number': '2',
                  'company_type': 'Manufacturing'}
        self.builder.update_basic_information_for(self.application, **params)
        eq_('New Company Name', self.application.company.name)
        eq_('2', self.application.company.registration_number)
        eq_('Manufacturing', self.application.company.company_type)

    @raises(FormIncompleteException)
    def test_update_application_status_should_fail_if_place_of_origin_is_not_present(self):
        params = {'status': 'Pending'}
        self.builder.update_basic_information_for(self.application, **params)

    def test_update_application_status_should_raise_errors_if_place_of_origin_is_not_present(self):
        params = {'status': 'Pending'}
        try:
            self.builder.update_basic_information_for(self.application, **params)
        except FormIncompleteException, (exception_instance):
            self.assertIsNotNone(exception_instance.error)
            self.assertIsNotNone(exception_instance.error.place_of_origin)
            eq_('New', self.application.status)

    @raises(FormIncompleteException)
    def test_update_application_status_should_fail_if_telephone_numbers_is_None(self):
        params = {'status': 'Pending'}
        self.builder.update_basic_information_for(self.application, **params)

    def test_update_application_status_should_raise_errors_if_telephone_number_is_None(self):
        params = {'status': 'Pending'}
        try:
            self.builder.update_basic_information_for(self.application, **params)
        except FormIncompleteException, (exception_instance):
            self.assertIsNotNone(exception_instance.error)
            self.assertIsNotNone(exception_instance.error.telephone_number)

    def test_should_be_able_to_change_status_if_form_is_complete(self):
        self.__update_basic_information__()
        eq_('Pending', self.application.status)

    def test_add_consultant(self):
        params = {'first_name': 'Consultant', 'last_name': 'Last'}
        self.builder.save_consultant_for(self.application_id, **params)
        eq_('Consultant', self.application.consultant.first_name)
        eq_('Last', self.application.consultant.last_name)
        self.repository.get.assert_called_once_with(self.application_id)
        self.repository.persist.assert_called_once_with(self.application)

    def test_update_consultant(self):
        params = {'first_name': 'Consultant', 'last_name': 'Last'}
        self.builder.save_consultant_for(self.application, **params)
        params = {'first_name': 'New Consultant'}
        self.builder.save_consultant_for(self.application, **params)
        eq_('New Consultant', self.application.consultant.first_name)

    def test_create_application(self):
        self.builder.create_application(**self.params)
        eq_('Company', self.application.company.name)
        eq_('30/Jan/2012', self.application.get_submission_date())

    def __update_basic_information__(self):
        params = {'telephone_number': '1234',
                  'fax_number': '5678',
                  'cable_number': '5677'}
        self.application.company.update_contact_information(**params)
        location_params = {'state': 'Cayo', 'country': 'Belize'}
        self.application.update_place_of_origin(**location_params)
        status_params = {'status': 'Pending'}
        self.builder.update_basic_information_for(self.application, **status_params)

    def __add_investor__(self):
        Investor = namedtuple("Investor", "first_name last_name percent_owned")
        investor = Investor(first_name="First", last_name="Investor", percent_owned=50)
        application_investor = self.builder.add_investor_to(self.application, investor)
        return application_investor

    def __add_background_checks(self):
        self.passed_background_check = InvestorBackgroundCheck("Passed", "P")
        self.failed_background_check = InvestorBackgroundCheck("Failed", "F")
