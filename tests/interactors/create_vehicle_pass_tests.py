import unittest
from nose.tools import eq_

from tests.persistence.repository.doubles import (VehiclePassRepositoryDouble,
                                                  CompanyRepositoryDouble)

from cfzcore.interactors.vehicle_pass import (create_vehicle_pass,
                                              fetch_all_vehicle_passes,
                                              fetch_all_vehicle_passes_for_company)
from cfzcore.person import Person
from cfzcore.company import Company
from collections import namedtuple


class CreateVehiclePassTests(unittest.TestCase):

    def setUp(self):
        self.company = self._create_company()
        self.repository = VehiclePassRepositoryDouble()

    def tearDown(self):
        VehiclePassRepositoryDouble.reset_pk()
        VehiclePassRepositoryDouble.reset_passes()

    def test_vehicle_pass_is_created(self):
        vehicle_pass = self._create_vehicle_pass(self.company)
        self.assertIsNotNone(vehicle_pass.id)

    def _create_vehicle_pass(self, company):
        pass_values = dict(company_id=company.id,
                           owner=u"Owner",
                           vehicle_year=u"2001",
                           vehicle_model=u"Mazda",
                           color="Blue",
                           license_plate=u"123123",
                           date_issued=u"21/09/2012",
                           sticker_number=u"00100",
                           receipt_number=u"0001",
                           month_expired=u"Dec")
        vehicle_pass = create_vehicle_pass(self.repository, **pass_values)
        return vehicle_pass

    def _create_company(self, name="Name"):
        manager = Person("Firstname", "Lastname")
        CompanyTuple = namedtuple('CompanyTuple', 'manager name\
                                   registration_number company_type')
        company_tuple = CompanyTuple(manager=manager, name=name,
                                     registration_number='12311',
                                     company_type='Import/Export')
        company = Company(company_tuple)
        repository = CompanyRepositoryDouble()
        company = repository.persist(company)
        return company
