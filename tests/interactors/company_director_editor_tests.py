import unittest
from nose.tools import eq_
from cfzcore.interactors.company import CompanyDirectorEditor


class CompanyDirectorEditorTests(unittest.TestCase):

    def test_should_add_director_to_empty_list(self):
        company = self._create_company("Company With only one  director", '1')
        self._clear_directors(company)
        self._add_director("FirstName", "LastName", "01/01/2012", company)
        eq_(1, len(company.directors))

    def test_should_modify_a_director_basic_info(self):
        _director = self._create_director("First", "Last", "01/01/2012")
        director_editor = CompanyDirectorEditor()
        director = director_editor.modify(_director, 
                                        **dict(first_name="New First Name",
                                              last_name="Last"))
        eq_("New First Name", director.get_first_name())

    def _add_director(self, first_name, last_name, start_date, company):
        '''Add a director to a company.
        '''
        director = self._create_director(first_name, last_name, start_date)
        director_editor = CompanyDirectorEditor()
        return director_editor.add_to_company(company, director)

    def _get_current_director(self, company):
        director_editor = CompanyDirectorEditor()
        return director_editor.get_current_director(company)

    def _create_director(self, first_name, last_name, start_date):
        '''Create a director
        '''
        from datetime import datetime
        from cfzcore.company import Director
        from cfzcore.person import Person
        return Director(Person(first_name, last_name), datetime.strptime(start_date, "%d/%m/%Y"))

    def _create_company(self, name, registration_number):
        '''Create  a company
        '''
        from cfzcore.company import (Company, CompanyParams, CompanyType)
        params = CompanyParams(name=name, registration_number=registration_number)
        return Company(params)

    def _clear_directors(self, company):
        if getattr(company, 'directors', None):
            del company.directors[:]
