from cfzcore.persistence.repository import (PersonRepository,
                                            ApplicationRepository,
                                            CompanyAbstractRepository,
                                            VehiclePassAbstractRepository)


class PersonRepositoryDouble(PersonRepository):

    __slots__ = ('pk', 'people', 'persist')

    _pk = 0
    _people = []

    @classmethod
    def reset_people(cls):
        del PersonRepositoryDouble._people[:]

    @classmethod
    def reset_pk(cls):
        PersonRepositoryDouble._pk = 0

    @property
    def pk(self):
        PersonRepositoryDouble._pk += 1
        return PersonRepositoryDouble._pk

    @property
    def people(self):
        return PersonRepositoryDouble._people

    def persist(self, person):
        if getattr(person, 'id', None) is None:
            setattr(person, 'id', self.pk)
        self.people.append(person)
        return person


class CompanyRepositoryDouble(CompanyAbstractRepository):

    __slots__ = ('pk', 'companies', 'persist', 'find_by_id',
                 '_persist_manager')

    _pk = 0
    _companies = []

    @classmethod
    def reset_pk(cls):
        CompanyRepositoryDouble._pk = 0
        PersonRepositoryDouble._pk = 0

    @classmethod
    def reset_companies(cls):
        del CompanyRepositoryDouble._companies[:]
        del PersonRepositoryDouble._people[:]

    @property
    def pk(self):
        CompanyRepositoryDouble._pk += 1
        return CompanyRepositoryDouble._pk

    @property
    def companies(self):
        return CompanyRepositoryDouble._companies

    def persist(self, company):
        if getattr(company, "id", None) is None:
            setattr(company, "id", self.pk)
        self.companies.append(company)
        return company

    def _persist_manager(self, manager):
        repository = PersonRepositoryDouble()
        return repository.persist(manager)

    def find_by_id(self, id):
        try:
            return [company for company in self.companies if company.id == id][0]
        except IndexError:
            return u'Company with this id does not exist!'


class ApplicationRepositoryDouble(ApplicationRepository):

    __slots__ = ['pk', 'applications', 'persist', '_persist_company',
                 '_persist_person', 'find_by_id']

    _pk = 0
    _applications = []

    @classmethod
    def reset_pk(cls):
        ApplicationRepositoryDouble._pk = 0
        CompanyRepositoryDouble.reset_pk()
        CompanyRepositoryDouble.reset_companies()
        PersonRepositoryDouble.reset_pk()
        PersonRepositoryDouble.reset_people()

    @classmethod
    def reset_applications(cls):
        del ApplicationRepositoryDouble._applications[:]
        CompanyRepositoryDouble.reset_companies()
        PersonRepositoryDouble.reset_people()

    @property
    def pk(self):
        ApplicationRepositoryDouble._pk += 1
        return ApplicationRepositoryDouble._pk

    @property
    def applications(self):
        return ApplicationRepositoryDouble._applications

    def persist(self, application):
        if getattr(application, 'id', None) is None:
            setattr(application, 'id', self.pk)
        application.manager = self._persist_person(application.manager)
        application.company = self._persist_company(application.company)
        self.applications.append(application)
        return application

    def find_by_id(self, id):
        try:
            return [application for application in self.applications if application.id == id]
        except IndexError:
            return u'Application does not exist with this id!'

    def _persist_company(self, company):
        company_repository = CompanyRepositoryDouble()
        return company_repository.persist(company)

    def _persist_person(self, person):
        person_repository = PersonRepositoryDouble()
        return person_repository.persist(person)


class VehiclePassRepositoryDouble(VehiclePassAbstractRepository):

    __slots__ = ['pk', 'passes', 'persist', 'all',
                 'all_for_company', 'find_company_by_id']

    _pk = 0
    _passes = []

    @classmethod
    def reset_pk(cls):
        VehiclePassRepositoryDouble._pk = 0
        CompanyRepositoryDouble.reset_pk()

    @classmethod
    def reset_passes(cls):
        del VehiclePassRepositoryDouble._passes[:]
        CompanyRepositoryDouble.reset_companies()

    @property
    def pk(self):
        VehiclePassRepositoryDouble._pk += 1
        return VehiclePassRepositoryDouble._pk

    @property
    def passes(self):
        return VehiclePassRepositoryDouble._passes

    def persist(self, vehicle_pass):
        if getattr(vehicle_pass, 'id', None) is None:
            setattr(vehicle_pass, 'id', self.pk)
        self.passes.append(vehicle_pass)
        return vehicle_pass

    def all(self):
        return self.passes

    def all_for_company(self, company):
        return [vehicle_pass for vehicle_pass in self.passes if vehicle_pass.company == company]

    def find_company_by_id(self, id):
        repository = CompanyRepositoryDouble()
        return repository.find_by_id(id)

    def search_by_owner(self, owner):
        pass
