import unittest
from nose.tools import raises

from mock import Mock

from cfzcore.approvers import ApplicationApprover
from cfzcore.approvers import NoInvestorsException
from cfzcore.approvers import InvestorBackgroundCheckEmptyException
from cfzcore.approvers import InvestorBackgroundCheckFailed


class ApplicationApproverTests(unittest.TestCase):

    def setUp(self):
        self.investor1 = Mock()
        self.investor1.background_check = Mock()
        self.investor1.has_passed_background_check.return_value = True
        self.approver = ApplicationApprover()
        self.application = Mock()

    def test_approve_an_application_form(self):
        self.application.investors = [self.investor1]
        self.approver.approve_application(self.application)
        self.application.approve.assert_called_once_with()

    @raises(NoInvestorsException)
    def test_approving_application_without_investors_fails(self):
        self.application.investors = []
        self.approver.approve_application(self.application)

    @raises(InvestorBackgroundCheckEmptyException)
    def test_approval_fails_if_investors_dont_have_background_check(self):
        self.application.mock_reset()
        investors = self.__create_investors_without_background_checks__()
        self.application.investors = investors
        self.approver.approve_application(self.application)

    @raises(InvestorBackgroundCheckFailed)
    def test_approval_fails_if_any_investor_background_check_fails(self):
        self.application.mock_reset()
        investors = self. __create_investors_with_failed_background_checks__()
        self.application.investors = investors
        self.approver.approve_application(self.application)

    def test_deny_application(self):
        self.application.mock_reset()
        self.approver.deny_application(self.application, "Reason for denial")
        self.application.deny.assert_called_once_with("Reason for denial")

    def __create_investors_without_background_checks__(self):
        investor1 = Mock()
        investor1.name.return_value = "Investor1"
        investor1.background_check = None
        investor2 = Mock()
        investor2.name.return_value = "Investor2"
        investor2.background_check = None
        return [investor1, investor2]

    def __create_investors_with_failed_background_checks__(self):
        investor1 = Mock()
        investor1.has_passed_background_check.return_value = False
        investor1.name.return_value = "Investor1"
        return [investor1]
