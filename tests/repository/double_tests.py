import unittest
from tests.repository_interface_tests import RepositoryInterfaceTests
from tests.repository import RepositoryDouble


class RepositoryDoubleTests(unittest.TestCase, RepositoryInterfaceTests):
    def setUp(self):
        self.repository = RepositoryDouble()
