import unittest
from nose.tools import eq_
from mock import MagicMock

from cfzcore.company.passes import CourtesyPass


class CourtesyPassTests(unittest.TestCase):

    def test_it_should_create_a_courtesy_pass(self):
        company = MagicMock()
        params = dict(company = company,
                      person = u"Person",
                      vehicle_year = u"2011",
                      vehicle_model = u"Mazda",
                      color = u"Blue",
                      license_plate = u"123123",
                      date_issued = u"21/09/2012",
                      sticker_number = u"001100",
                      month_expired = u"Dec",
                      issued_by = u"Jane Smith",
                      authorized_by = u"Mr. CEO")
        courtesy_pass = CourtesyPass.create(**params)
        eq_("Person", courtesy_pass.person)
        eq_("21/09/2012", courtesy_pass.date_issued.strftime('%d/%m/%Y'))
        company.add_courtesypass.assert_called_once_with(courtesy_pass)

