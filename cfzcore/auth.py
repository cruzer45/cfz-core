def groupfinder(user_account):
    if user_account and hasattr(user_account, 'groups'):
        return ['g:%s' % group.group_name for group in user_account.groups]
