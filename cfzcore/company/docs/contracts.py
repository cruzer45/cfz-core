from datetime import datetime


class OperationalContract(object):
    '''Represents an Operational Contract.
    
    Attributes:
    - cfz_ceo
    - director
    - cfz_office address
    - compny_types
    - goods
    - seal_date
    - processing_fee
    '''

    @staticmethod
    def create_with(**kwargs):
        '''Create an Operational Contract with a map of values.
        '''
        opcontract = OperationalContract()
        seal_date = datetime.strptime(kwargs['seal_date'], '%d/%m/%Y')
        setattr(opcontract, 'company',          kwargs.get('company'))
        setattr(opcontract, 'cfz_ceo',          kwargs.get('cfz_ceo'))
        setattr(opcontract, 'director',         kwargs.get('director'))
        setattr(opcontract, 'cfz_office',        kwargs.get('cfz_office'))
        setattr(opcontract, 'company_types',    kwargs.get('company_types'))
        setattr(opcontract, 'goods',            kwargs.get('goods'))
        setattr(opcontract, 'seal_date',        seal_date)
        setattr(opcontract, 'processing_fee',   kwargs.get('processing_fee'))
        return opcontract
