from collections import namedtuple
from cfzcore.shareholder import Shareholder
from cfzcore.address import Address
from datetime import datetime


CompanyTuple = namedtuple("CompanyTuple", "id name cell_number\
      telephone_number fax_number deleted\
      description_of_goods state country created_date\
      registration_number active\
      date_sent_letter_of_acceptance")

CompanyAnnualReportTuple = namedtuple("CompanyAnnualReportTuple", "id\
      company_id date_submitted reporting_year")


class CompanyStatus(object):
    '''Enum for company status.
     A company can transition through different
     status during its stay at the CFZ. These are
     the possible status they can be in.
     '''
    ACTIVE = "Active"
    INACTIVE = "Inactive"


class Company(object):

    def __init__(self, params):
        self.__validate__(params)
        self.registration_number = params.registration_number
        #self.company_type = params.company_type
        self.name = params.name
        self.created_date = datetime.utcnow()

    def __validate__(self, params):
        if params.name is None or len(params.name) < 1:
            raise InvalidNameException("Name can not be empty!")
        if params.registration_number is None or len(params.registration_number) < 1:
            raise InvalidNameException("Registration Number can not be empty!")

    def assign_shareholder(self, **kwargs):
        if 'person' in kwargs:
            kwargs['first_name'] = kwargs['person'].first_name
            kwargs['last_name'] = kwargs['person'].last_name
        _shareholder = Shareholder(**kwargs)
        if getattr(self, 'shareholders', None) is None:
            setattr(self, 'shareholders', [])
        self.shareholders.append(_shareholder)

    def update_contact_information(self, **kwargs):
        if 'telephone_number' in kwargs.keys():
            self.telephone_number = kwargs['telephone_number']
        if 'fax_number' in kwargs.keys():
            self.fax_number = kwargs['fax_number']
        if 'cell_number' in kwargs.keys():
            self.cell_number = kwargs['cell_number']

    def shareholder_shares_for(self, person):
        for shareholder in self.shareholders:
            if shareholder.person.name() == person.name():
                return  shareholder.shares

    def replace_director(self, director):
        '''Replace a company's director.
        '''
        self.director = director

    def update_general_information(self, **kwargs):
        '''
        Update the company's general information. It takes a dictionary as a
        parameter with the information that needs to be updated. The keys
        that this dictionary can take are:
        registration_number, name, company_type
        '''
        if 'name' in kwargs.keys():
            self.name = kwargs['name']
        if 'registration_number' in kwargs.keys():
            self.registration_number = kwargs['registration_number']
        if 'company_type' in kwargs.keys():
            self.company_type = kwargs['company_type']

    def get_created_date(self):
        if self.created_date is not None:
            return self.created_date.strftime("%d/%m/%Y")
        else:
            return None

    def update_country(self, **kwargs):
        self.state = kwargs['state']
        self.country = kwargs['country']

    def update_address(self, line1, line2 ):
        """Updates the country's address

        It returns an instance of company with the updated address.
        """
        if self.address is None:
            self.address = Address()
        self.address.line1 = line1
        self.address.line2 = line2
        return self.company

    def update_contact_numbers(self, **kwargs):
        self.telephone_number = kwargs.get('telephone_number')
        self.cell_number = kwargs.get('cell_number')
        self.fax_number = kwargs.get('fax_number')

    def add_director(self, director):
        self.directors = getattr(self, 'directors', [])
        self.directors.append(director)

    def add_courtesypass(self, courtesypass):
        self.courtesy_passes = getattr(self, 'courtesy_passes', [])
        self.courtesy_passes.append(courtesypass)

    @property
    def __tuple__(self):
        company_tuple = CompanyTuple(id=getattr(self, "id", None),
            name=self.name,
            cell_number=getattr(self, 'cell_number', ''),
            fax_number=getattr(self, 'fax_number', ''),
            telephone_number=getattr(self, 'telephone_number', ''),
            description_of_goods=getattr(self, 'description_of_goods', ''),
            state=getattr(self, 'state', ''),
            country=getattr(self, 'country', ''),
            registration_number=self.registration_number,
            date_sent_letter_of_acceptance=getattr(self, "date_sent_letter_of_acceptance", None),
            active=getattr(self, "active", False),
            deleted=getattr(self, "deleted", False),
            created_date=getattr(self, 'created_date', None))
        return company_tuple


class CompanyAnnualReport(object):
    def __init__(self, **kwargs):
        self.company = kwargs['company']
        self.date_submitted = datetime.strptime(kwargs['date_submitted'], "%d/%m/%Y")
        self.reporting_year = kwargs['reporting_year']

    @property
    def __tuple__(self):
        report_tuple = CompanyAnnualReportTuple(id=getattr(self, "id", None),
           company_id=self.company.id,
           date_submitted=self.date_submitted,
           reporting_year=self.reporting_year)
        return report_tuple


class InvalidNameException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class NullDirectorException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class CompanyType(object):
    '''Categorizes a company.
    A company can have more than one type. 
    
    These can be:
        Import/Export
        Manufacturing
        Rental of Space
        Telecommunications
        Radio
        Banking
        Gaming
        ISP
        Freight Services
    '''

    @property
    def name(self):
        return getattr(self, '_name', None)

    @name.setter
    def name(self, value):
        self._name = value

    @staticmethod
    def create(name):
        company_type = CompanyType()
        company_type.name = name
        return company_type
        

CompanyParams = namedtuple("CompanyParams", "name registration_number")

class Director(object):
    end_date = None

    def __init__(self, person, start_date):
        self.person = person
        self.start_date = start_date

    def get_start_date(self):
        return self.start_date.strftime("%d/%m/%Y")

    def add_end_date(self, end_date):
        self.end_date = end_date

    def get_first_name(self):
        return self.person.first_name


class RepresentationType(object):
    '''The type of represenation for a company authorized representative.'''
    def __init__(self, name):
        self.name = name


class AuthorizedRepresentative(object):
    def __init__(self):
        pass

    @property
    def signature(self):
        return getattr(self, '_signature', None)

    @signature.setter
    def signature(self, value):
        self._signature = value

    @property
    def first_name(self):
        return self.person.first_name

    @property
    def last_name(self):
        return self.person.last_name

    @property
    def name(self):
        return self.person.name()


    @staticmethod
    def create(**kwargs):
        from cfzcore.person import (Person, InvalidNumberOfNameArgumentsException)
        try:
            person = Person(kwargs.get('first_name'), kwargs.get('last_name'))
        except InvalidNumberOfNameArgumentsException as ex:
            raise IllegalPersonNameException("Could not create Person Instance")

        representative = AuthorizedRepresentative()
        representative.person = person
        if kwargs.get('company') is None:
            raise NullCompanyException("Did not provide a valid company!") 
        else:
            representative.company = kwargs.get('company')
        if kwargs.get('representation_type') is None:
            raise NullRepresentationTypeException("Did not provide a valid RepresentationType!")
        else:
            representative.representation_type = kwargs.get('representation_type')
        return representative


class AuthorizedRepresentativeException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class IllegalPersonNameException(AuthorizedRepresentativeException):
    pass


class NullCompanyException(AuthorizedRepresentativeException):
    pass

class NullRepresentationTypeException(AuthorizedRepresentativeException):
    pass
