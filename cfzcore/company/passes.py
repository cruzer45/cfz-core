from datetime import datetime


class CourtesyPass(object):
    @property
    def person(self):
        return self._person

    @person.setter
    def person(self, value):
        self._person = value

    @property
    def issued_by(self):
        return self._issued_by

    @issued_by.setter
    def issued_by(self, value):
        self._issued_by = value

    @property
    def authorized_by(self):
        return self._authorized_by

    @authorized_by.setter
    def authorized_by(self, value):
        self._authorized_by = value

    @staticmethod
    def create(**kwargs):
        courtesy_pass = CourtesyPass()
        courtesy_pass.person = kwargs.get('person', None)
        courtesy_pass.vehicle_year = kwargs.get('vehicle_year', None)
        courtesy_pass.vehicle_model = kwargs.get('vehicle_model', None)
        courtesy_pass.color = kwargs.get('color', None)
        courtesy_pass.license_plate = kwargs.get('license_plate', None)
        courtesy_pass.date_issued = datetime.strptime(kwargs['date_issued'], '%d/%m/%Y')
        courtesy_pass.sticker_number = kwargs.get('sticker_number', None)
        courtesy_pass.month_expired = kwargs.get('month_expired', None)
        courtesy_pass.issued_by = kwargs.get('issued_by', None)
        courtesy_pass.authorized_by = kwargs.get('authorized_by', None)
        company = kwargs.get('company', None)
        company.add_courtesypass(courtesy_pass)
        return courtesy_pass
