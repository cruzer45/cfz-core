from cryptacular import bcrypt


class UserAccount(object):
    crypt = bcrypt.BCRYPTPasswordManager()
    groups = []

    def __init__(self, username, email):
        self.username = username
        self.email = email

    def check_password(self, passphrase):
        return self.crypt.check(self.passphrase, passphrase)

    def set_email(self, email):
        self.email = email

    def hash_password(self, passphrase):
        return unicode(self.crypt.encode(passphrase))

    def set_password(self, value):
        self.passphrase = self.hash_password(value)
        return self.passphrase

    def add_to_group(self, group):
        self.groups.append(group)
        return self.groups


class UserGroup(object):
    users = []
    
    def __init__(self, name):
        self.group_name = name

    @staticmethod
    def create(name):
        return UserGroup(name)
