class InvestorBackgroundCheck(object):
    def __init__(self, name, code):
        self.name = name
        self.code = code


class InvestorBackgroundChecker(object):
    '''Background Checker for Investor'''

    def __init__(self, repository):
        self.repository = repository

    def save_background_check(self, investor_id, background_check_result):
        investor = self.repository.get(investor_id)
        investor.do_background_check(background_check_result)
        self.repository.persist(investor)
        return investor.name()


def enum(**enums):
    return type('Enum', (), enums)
