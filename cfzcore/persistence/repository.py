import abc


class VehiclePassAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, vehicle_pass):
        """Persists a vehicle pass"""
        raise NotImplementedError(
            "Subclass should implement persist(vehicle_pass)!")

    @abc.abstractmethod
    def all(self):
        """Return all the vehicle passes"""
        raise NotImplementedError("Subclass should implement all()!")

    @abc.abstractmethod
    def all_for_company(self, company):
        """Return all vehicle passes for aa company"""
        raise NotImplementedError(
            "Subclass should implement all_for_company()!")

    @abc.abstractmethod
    def find_company_by_id(self, id):
        """Retrieve a company"""
        raise NotImplementedError(
            "Subclass should implement find_company_by_id!")

    @abc.abstractmethod
    def search_by_owner(self, owner):
        raise NotImplementedError(
            "Subclass should implement search_by_owner!")


class PersonRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def persist(self, person):
        """Persist a person"""
        raise NotImplementedError(
            "Subclass should implement persist(person)!")


class ApplicationRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def persist(self, application):
        """Persist an application """
        raise NotImplementedError(
            "Subclass should implement persist(application)!")

    @abc.abstractproperty
    def find_by_id(self, id):
        """Find an Application by id """
        raise NotImplementedError("Subclass should implement find_by_id(id)!")


class CompanyAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def persist(self, company):
        """Persist a company"""
        raise NotImplementedError(
            "Subclass should implement persist(company)!")

    @abc.abstractproperty
    def find_by_id(self, id):
        """Find a company by id"""
        raise NotImplementedError("Subclass should implement find_by_id(id)!")


class GoodsCategoryAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, category):
        """Persist a goods category"""
        raise NotImplementedError("Subclass should implement persist()!")

    @abc.abstractmethod
    def all(self):
        """List all goods categories"""
        raise NotImplementedError("Subclass should implement all()!")

    @abc.abstractmethod
    def get(self, id):
        """Find a category with the given id"""
        raise NotImplementedError("Subclass should implement find_by_id(id)!")


class OutletsAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, outlet):
        """Persist a goods outlet"""
        raise NotImplementedError("Subclass should implement persist(outlet)!")

    @abc.abstractmethod
    def all_for_company(self, company):
        """List all outlets"""
        raise NotImplementedError(
            "Subclass should implement all_for_company()!")

    @abc.abstractmethod
    def find_by_id(self, id):
        """Find an outlet with the given id"""
        raise NotImplementedError("Subclass should implement find_by_id(id)!")


class OutletVehiclepassAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, vehicle_pass):
        """Persists a vehicle pass"""
        raise NotImplementedError("Subclass should implement persist()!")

    @abc.abstractmethod
    def all(self):
        """Return all the vehicle passes"""
        raise NotImplementedError("Subclass should implement all()!")

    @abc.abstractmethod
    def all_for_outlet(self, outlet):
        """Return all vehicle passes for an outlet"""
        raise NotImplementedError(
            "Subclass should implement all_for_outlet()!")

    @abc.abstractmethod
    def get(self, id):
        """Retrieve a vehile pass"""
        raise NotImplementedError("Subclass should implement get(id)!")

    @abc.abstractmethod
    def search_by_owner(self, owner):
        raise NotImplementedError("Subclass should implement search_by_owner!")


class AuthorizedRepresentativeAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, authorized_representative):
        '''Persists an authorized representative for a company.'''
        raise NotImplementedError("Subclass should implement persist()!")

    @abc.abstractmethod
    def all_for_company(self, company):
        '''Retrieves all authorized representatives for a company.
        '''
        raise NotImplementedError(
            "Subclass should implement all_for_company()!")

    @abc.abstractmethod
    def get(self, id):
        '''Retrieves an instance from the data store.'''
        raise NotImplementedError("Subclass should impelement get()!")


class RepresentationTypeAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, representation_type):
        '''Persists a representation type.'''
        raise NotImplementedError("Subclass should implement persist()!")

    @abc.abstractmethod
    def all(self):
        '''Retrieves all representation types.'''
        raise NotImplementedError("Subclass should implement all()!")

    @abc.abstractmethod
    def get(self, id):
        '''Retrieves a representation type with specified id.'''
        raise NotImplementedError("Subclass should implement get(id)!")


class CourtesyPassAbstractRepository(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, courtesy_pass):
        '''Persists a courtesy pass.'''
        raise NotImplementedError('Subclass should implement persist()!')

    @abc.abstractmethod
    def all_for_company(self, company):
        '''Retrieves all courtesy passes for a company.'''
        raise NotImplementedError(
            'Subclass should implement all_for_company()!')

    @abc.abstractmethod
    def get(self, id):
        '''Retrieves a courtesy pass with the specified id.'''
        raise NotImplementedError('Subclass should implement get()!')


class UserAccountAbstractRepository(object):
    '''Interface for UserAccountRepository.'''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, user_account):
        '''Persists a user account.'''
        raise NotImplementedError('Subclass should implement persist()!')

    @abc.abstractmethod
    def all(self):
        '''Lists all user accounts.'''
        raise NotImplementedError('Subclass should implement all()!')

    @abc.abstractmethod
    def get(self, id):
        '''Retrieves a user account with the specified id.'''
        raise NotImplementedError('Subclass should implement get()!')

    @abc.abstractmethod
    def find(self, email):
        '''Retrieves a user account with specified email.'''
        raise NotImplementedError('Subclass should implement find()!')

    @abc.abstractmethod
    def is_email_available(self, email):
        '''Checks if there is an account with the specified email.'''
        raise NotImplementedError(
            'Subclass should impelment is_email_available()!')

    @abc.abstractmethod
    def remove_permissions(self, permissions):
        '''Removes the permissions from the user account.'''
        raise NotImplementedError(
            'Subclass should implement remove_permissions()!')


class AccessLetterAbstractRepository(object):
    '''Interface for the Access Letter Repository'''
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def persist(self, letter):
        '''Persists a letter.'''
        raise NotImplementedError('Subclass should implement persist()!')

    @abc.abstractmethod
    def get(self, id):
        '''Retrieves a letter with the specified id.'''
        raise NotImplementedError('Subclass should implement get()!')

    @abc.abstractmethod
    def list_by_letter_type(self, letter_type, offset, max_records):
        '''Retrieves a list of letters by letter type.'''
        raise NotImplementedError(
            'Subclass should implement list_by_letter_type()!')

    @abc.abstractmethod
    def list_expired_letters(self, letter_type, offset, max_records):
        '''Retrieves a list of expired letters.'''
        raise NotImplementedError(
            'Subclass should implement list_expired_letters()!')

    @abc.abstractmethod
    def count_by_letter_type(self, letter_type, expired):
        '''Counts the total number of access letters.'''
        raise NotImplementedError(
            'Subclass should implement count_by_letterType()!')
