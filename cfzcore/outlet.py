from cfzcore.person import Person


class OutletStatus(object):
    '''Enum representing an Outlet's Status.

    '''
    ACTIVE = 'Active'
    INACTIVE = 'INACTIVE'


class Outlet(object):
    '''Object that represents an outlet.

    '''

    managers = []
    max_vehicles = 0

    def __init__(self, name):
        self.name = name


class OutletManager(object):
    def __init__(self, first, last):
        self.set_names(first, last)

    def set_names(self, first, last):
        self.person = self.person if getattr(self, 'person', None) is not None else Person(first, last)


class OutletVehiclepass(object):
    def __init__(self, **kwargs):
        setattr(self, 'owner', kwargs['owner'])
        setattr(self, 'outlet', kwargs['outlet'])
        setattr(self, 'vehicle_year', kwargs['vehicle_year'])
        setattr(self, 'vehicle_model', kwargs['vehicle_model'])
        setattr(self, 'color', kwargs['color'])
        setattr(self, 'license_plate', kwargs['license_plate'])
        setattr(self, 'date_issued', kwargs['date_issued'])
        setattr(self, 'sticker_number', kwargs['sticker_number'])
        setattr(self, 'receipt_number', kwargs['receipt_number'])
        setattr(self, 'month_expired', kwargs['month_expired'])
        setattr(self, 'issued_by', kwargs['issued_by'])
