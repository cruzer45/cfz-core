from datetime import datetime
from collections import namedtuple


ApplicationTuple = namedtuple("ApplicationTuple", "id description_of_goods\
                               submission_date consultant state country\
                               created_date approved_date denied_date\
                               reason_for_denial status company")


class Status():

    NEW = "New"
    DENIED = "Denied"
    APPROVED = "Approved"
    PENDING = "Pending"


class Application(object):

    def __init__(self, **kwargs):
        for arg, value in kwargs.items():
            if arg == 'submission_date':
                self.set_submission_date(value)
            else:
                setattr(self, arg, value)

    def assign_investor(self, person, percent_owned):
        application_investor = ApplicationInvestor(person, percent_owned, self)
        if getattr(self, 'investors', None) is None:
            setattr(self, 'investors', [])
        self.investors.append(application_investor)
        return application_investor

    def remove_all_investors(self):
        if getattr(self, 'investors', None):
            del self.investors[:]

    def total_number_of_investors(self):
        return len(self.investors)

    def remove_investor(self, investor):
        for inv in self.investors:
            if investor == inv:
                self.investors.remove(inv)

    def set_place_of_origin(self, state, country):
        '''
        Insert the place of origin. Should not be used
        to update existing values.
        '''
        self.state = state
        self.country = country

    def update_place_of_origin(self, **kwargs):
        if 'state' in kwargs.keys():
            self.state = kwargs['state']
        if 'country' in kwargs.keys():
            self.country = kwargs['country']

    def place_of_origin(self):
        return self.state + ", " + self.country

    def is_place_of_origin_present(self):
        present = False
        if getattr(self, 'state', None) is not None and getattr(self, 'country', None) is not None:
            present = True
        return present

    def set_submission_date(self, date_string):
        self.submission_date = datetime.strptime(date_string, "%d/%m/%Y")

    def get_submission_date(self):
        if self.submission_date:
            return self.submission_date.strftime("%d/%b/%Y")
        else:
            return None

    def get_created_date(self):
        if self.created_date:
            return self.created_date.strftime("%d/%b/%Y")
        else:
            return None

    def deny(self, reason_for_denial):
        self.reason_for_denial = reason_for_denial
        self.status = Status.DENIED
        self.denied_date = datetime.today()

    def approve(self):
        self.status = Status.APPROVED
        self.copy_approved_data_to_company()
        self.approved_date = datetime.today()

    def copy_approved_data_to_company(self):
        company = self.company
        company.active = True
        company.country = getattr(self, 'country', None)
        company.state = getattr(self, 'state', None)
        for investor in getattr(self, 'investors', []):
            params = dict(person=investor.person, shares=investor.percent_owned,
                          background_check=investor.background_check)
            company.assign_shareholder(**params)

    @property
    def __tuple__(self):
        company = self.company.__tuple__
        consultant = self.consultant.__tuple__ if getattr(self, 'consultant', None) else None
        application_tuple = ApplicationTuple(id=getattr(self, "id", None),
                                             company=company,
                                             consultant=consultant,
                                             state=getattr(self, 'state', None),
                                             country=getattr(self, 'country', None),
                                             description_of_goods=getattr(self, 'description_of_goods', None),
                                             created_date=getattr(self, 'created_date', None),
                                             submission_date=getattr(self, 'submission_date', None),
                                             approved_date=getattr(self, 'approved_date', None),
                                             denied_date=getattr(self, 'denied_date', None),
                                             reason_for_denial=getattr(self, 'reason_for_denial', None),
                                             status=getattr(self, 'status', None))
        return application_tuple

    @property
    def __dicts__(self):
        print "\ncompany.__dict__:", self.company
        manager = self.company.manager
        print "\nmanager: ", manager
        manager_dict = manager.__dict__
        #manager_dict = dict(id=getattr(self.company.manager, "id", None),
         #                   first_name=getattr(self.company.manager,
          #                                     "first_name", None),
           #                 last_name=getattr(self.company.manager, "last_name",
            #                                  None))
        company_dict = dict(id=getattr(self.company, "id", None),
                            name=self.company.name,
                            registration_number=self.company.registration_number,
                            company_type=self.company.company_type,
                            manager=manager_dict)
        location_dict = dict(state=self.state, country=self.country)
        consultant_dict = dict()
        if self.consultant:
            consultant_dict['id'] = getattr(self.consultant, "id", None)
            consultant_dict['first_name'] = self.consultant.first_name
            consultant_dict['last_name'] = self.consultant.last_name
        else:
            consultant_dict = None
        application_dict = dict(id=getattr(self, "id", None),
                                submission_date=self.get_submission_date(),
                                company=company_dict,
                                description_of_goods=self.description_of_goods,
                                location=location_dict,
                                status=self.status,
                                consultant=consultant_dict
                                )
        return application_dict


class ApplicationInvestor(object):
    background_check = None

    def __init__(self, person, percent_owned, application):
        self.person = person
        self.percent_owned = percent_owned
        self.application = application

    def name(self):
        return self.person.name()

    def has_passed_background_check(self):
        passed = False
        if self.background_check == 'P':
            passed = True
        return passed

    def do_background_check(self, background_check):
        self.background_check = background_check
