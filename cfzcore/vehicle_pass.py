from collections import namedtuple

VehiclePassTuple = namedtuple("VehiclePassTuple", "id owner company\
        vehicle_year vehicle_model color license_plate date_issued\
        sticker_number receipt_number month_expired")


class VehiclePass(object):
    def __init__(self, **kwargs):
        setattr(self, 'owner', kwargs['owner'])
        setattr(self, 'company', kwargs['company'])
        setattr(self, 'vehicle_year', kwargs['vehicle_year'])
        setattr(self, 'vehicle_model', kwargs['vehicle_model'])
        setattr(self, 'color', kwargs['color'])
        setattr(self, 'license_plate', kwargs['license_plate'])
        setattr(self, 'date_issued', kwargs['date_issued'])
        setattr(self, 'sticker_number', kwargs['sticker_number'])
        setattr(self, 'receipt_number', kwargs['receipt_number'])
        setattr(self, 'month_expired', kwargs['month_expired'])

    @property
    def __tuple__(self):
        company = {'id': self.company.id, 'name': self.company.name}
        vehicle_pass_tuple = VehiclePassTuple(id=getattr(self, "id", None),
                                              owner=getattr(self, 'owner', None),
                                              vehicle_year=getattr(self, 'vehicle_year', None),
                                              vehicle_model=getattr(self, 'vehicle_model', None),
                                              color=getattr(self, 'color', None),
                                              license_plate=getattr(self, 'license_plate', None),
                                              date_issued=getattr(self, 'date_issued', None),
                                              sticker_number=getattr(self, 'sticker_number', None),
                                              receipt_number=getattr(self, 'receipt_number', None),
                                              month_expired=getattr(self, 'month_expired', None),
                                              company=company)
        return vehicle_pass_tuple
