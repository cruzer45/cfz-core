from cfzcore.shareholder import Shareholder
from cfzcore.company import CompanyAnnualReport


def send_letter_of_acceptance(company_id, date_letter_sent, repository):
    company = repository.get(company_id)
    company.date_sent_letter_of_acceptance = date_letter_sent
    repository.persist(company)


def create_annual_report_for_company(company, **report):
    report['company'] = company
    annual_report = CompanyAnnualReport(**report)
    company_reports = getattr(company, "annual_reports", [])
    company_reports.append(annual_report)
    return annual_report


class CompanyCountry(object):
    """Facade for dealing with company country's of origin.

    """

    def __init__(self, company_id, repository):
        self.repository = repository
        self.company = self.repository.get(company_id)

    def update(self, **kwargs):
        self.company.update_country(**kwargs)
        self.repository.persist(self.company)
        return self.company


class CompanyContactNumbers(object):

    def __init__(self, company_id, repository):
        self.repository = repository
        self.company = self.repository.get(company_id)

    def update(self, **contact_numbers):
        self.company.update_contact_numbers(**contact_numbers)
        self.repository.persist(self.company)
        return self.company


class CompanyShareholder(object):
    """Save and update Company Sharholders"""

    def __init__(self, company_id, repository):
        self.repository = repository
        self.company_id = company_id
        self.company = repository.get(company_id)

    def total_shares_owned(self):
        total_shares = 0
        for shareholder in self.company.shareholders:
            total_shares += int(shareholder.shares)
        return total_shares

    def save_shareholder_by_id(self, shareholder_id, **kwargs):
        shareholder = self.repository.get_shareholder_by_id(shareholder_id)
        person = shareholder.person
        person.first_name = kwargs.get("first_name", person.first_name)
        person.last_name = kwargs.get("last_name", person.last_name)
        shareholder.background_check = kwargs.get("background_check",
                                                  shareholder.background_check)
        shareholder.shares = kwargs.get("shares", shareholder.shares)
        self.repository.persist(shareholder)
        return shareholder.__tuple__

    def add_shareholder(self, **kwargs):
        kwargs['company'] = self.company
        shareholder = Shareholder(**kwargs)
        self.company.shareholders.append(shareholder)
        self.repository.persist(self.company)
        return shareholder.__tuple__

    def all_shareholders(self):
        shareholders = self.repository.get_shareholders_for_company(self.company_id)
        return list(shareholder.__tuple__ for shareholder in shareholders if getattr(shareholder, 'active', True))


class CompanyDirectorEditor(object):
    def __init__(self):
        pass

    def add_to_company(self, company, director):
        '''Add a director to a company.
        Returns a company with the new list of directors.
        '''
        #if len(getattr(company, 'directors', [])) > 0:
        #    company.directors[-1].end_date = director.start_date
        company.add_director(director)
        director.company = company
        return director

    def get_current_director(self, company):
        for director in company.directors:
            if director.end_date is None:
                return director

    def modify(self, director, **kwargs):
        director.person.first_name = kwargs.get('first_name', director.person.first_name)
        director.person.last_name = kwargs.get('last_name', director.person.last_name)
        director.person.email = kwargs.get('email', None)
        director.person.phone1 = kwargs.get('phone1', None)
        director.person.phone2 = kwargs.get('phone2', None)
        return director
