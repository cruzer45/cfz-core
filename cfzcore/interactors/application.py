from cfzcore.person import Person
from cfzcore.interactors.application_builder import ApplicationFormBuilder


class ApplicationInvestorForm(object):
    def __init__(self, repository):
        self.repository = repository

    def add_investor_to(self, application_id, investor):
        application = self.repository.get(application_id)
        _investor = Person(investor.first_name, investor.last_name)
        application_investor = application.assign_investor(_investor, investor.percent_owned)
        self.repository.persist(application)
        return application_investor

    def get_all_investors_for(self, application_id):
        application = self.repository.get(application_id)
        return application.investors

    def remove_investor_from(self, application_id, investor_id):
        application = self.repository.get(application_id)
        investor = self.repository.get_investor(investor_id)
        application.remove_investor(investor)

    def add_background_check(self, **kwargs):
        investor = self.repository.get_investor(kwargs['investor_id'])
        investor.background_check = kwargs['background_check']


class ApplicationBasicInformation(object):

    def __init__(self, repository):
        self.repository = repository
        self.builder = ApplicationFormBuilder(self.repository)

    def update_basic_information(self, **kwargs):
        application = self.repository.get(kwargs['application_id'])
        self.builder.update_basic_information_for(application, **kwargs)
        self.repository.persist(application)
        return application.__tuple__

    def save_contact_numbers(self, **kwargs):
        application = self.repository.get(kwargs['application_id'])
        application.company.update_contact_information(**kwargs)
        self.repository.persist(application)
        return application.__tuple__

    def save_company_location(self, **kwargs):
        application = self.repository.get(kwargs['application_id'])
        application.update_place_of_origin(**kwargs)
        self.repository.persist(application)
        return application.__tuple__

    def save_description_of_goods(self, **kwargs):
        application = self.repository.get(kwargs['application_id'])
        application.description_of_goods = kwargs['description_of_goods']
        self.repository.persist(application)
