from cfzcore.person import Person
from collections import namedtuple


ShareholderTuple = namedtuple("ShareholderTuple", "id person shares company_id\
                               background_check")


class Shareholder(object):
    def __init__(self, **kwargs):
        self.person = Person(kwargs['first_name'], kwargs['last_name'])
        self.shares = kwargs['shares']
        self.company = kwargs.get('company', None)
        if 'background_check' in kwargs:
            self.background_check = kwargs['background_check']

    @property
    def first_name(self):
        return self.person.first_name

    @property
    def last_name(self):
        return self.person.last_name

    @first_name.setter
    def first_name(self, value):
        self.person.first_name = value

    @last_name.setter
    def last_name(self, value):
        self.person.last_name = value

    @property
    def __tuple__(self):
        shareholder_tuple = ShareholderTuple(id=getattr(self, "id", None),
                                             person=self.person.__tuple__,
                                             shares=getattr(self, "shares", 0),
                                             company_id=self.company.id,
                                             background_check=getattr(self, 'background_check', None))
        return shareholder_tuple
