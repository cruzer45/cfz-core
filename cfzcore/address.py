class Address(object):
    '''Represents an address in the CFZ.

    This is a CFZ-specific representation of a location within its
    geographic limits.
    '''

    def __init__(self):
        pass

    @property
    def coordinates(self):
        '''Returns the address' cooordinates.
        '''
        return "%d , %d" % (self.latitude, self.longitude)

    def update(self, line1, line2):
        '''Updates an address' values.

        It only updates line1 & line2. The lat & long are updated separately.
        line1 represents the Plaza.
        line2 represents the Locale.
        '''
        self.line1 = line1
        self.line2 = line2
        return self

    def __eq__(self, other):
        return self.line1 == getattr(other, 'line1', None) and self.line2 == getattr(other, 'line2', None)
